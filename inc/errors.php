<?php

$link = $_SERVER['PHP_SELF'];
$link_array = explode('/',$link);
$last_url = end($link_array);

//  VÝPIS ERROROV NA REGISTRÁCIÍ ALEBO PRIHLÁSOVANÍ

if ($last_url == "index.php" || $last_url == "login.php") {
    if (sizeof($errors) > 0) {
        ?>

        <div class="errors">
            <h3 class="errorWarning"><?= $value = reset($errors); ?></h3>
        </div>

        <?php
    }
}

//  VÝPIS ERROROV NA PRIDANÍ NOVEJ ÚLOHY

if ($last_url == "tasks.php") {
    if (sizeof($errors_add) > 0) {
        ?>

        <div class="errors">
            <h3 class="errorWarning"><?= $value = reset($errors_add); ?></h3>
        </div>

        <?php
    }
}

//  VÝPIS ERROROV NA ÚPRAVE ÚLOHY

if ($last_url == "edit.php") {
    if (sizeof($errors_edit) > 0) {
        ?>

        <div class="errors">
            <h3 class="errorWarning"><?= $value = reset($errors_edit); ?></h3>
        </div>

        <?php
    }
}
