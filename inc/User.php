<?php

class User{

    public $email_r = "";
    public $password_r = "";
    public $firstname = "";
    public $lastname = "";

    public function registerDataSet($newMail,$newPassword,$newFName,$newLName){
        $this->email_r = $newMail;
        $this->password_r = $newPassword;
        $this->firstname = $newFName;
        $this->lastname = $newLName;
    }

    public $email_l = "";
    public $password_l = "";

    public function loginDataSet($loginMail,$loginPass){
        $this->email_l = $loginMail;
        $this->password_l = $loginPass;
    }

    public function checkEmail($email) {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false && $find2 > $find1);
    }

    public function checkPassword($password) {
        $empty = false;
        if (empty($password)){
            $empty = true;
        }
        $length = strlen($password);
        return ($empty !== false && $length >= 8);
    }
}
