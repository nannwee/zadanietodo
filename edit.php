<?php require "inc/config.php" ?>

<?php
if (isset($_POST['edit'])){
    include "inc/edit-item.php";
}
?>


<?php include "inc/header.php" ?>

<nav class="navbar navbar-expand-md fixed-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarRes" >
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarRes">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="tasks.php">go back</a>
            </li>
        </ul>
    </div>
</nav>

<div id="top">


    <h2>Edit your task</h2>



    <div class="container-fluid ">



        <div class="content">
            <div class="tasks">

                <?php

                $sql = "SELECT * from tasks where taskID = '$_GET[id]'";
                $data = $conn->query($sql);

                require "inc/Task.php";

                $task = new Task();

                $userID = "";

                if ($data->num_rows > 0) {
                    while ($row = $data->fetch_assoc()) {

                        $userID = $row["userID"];

                        $task->setEdit($row["taskID"],$row["name"],$row["description"],$row["deadline"],$row["file"]);

                        ?>
                        <div class="task mb-4">
                            <div class="nazov col-md-12 mb-3">
                                <h4><strong><?=  $task->name ?></strong><small class="opacity"> - názov</small></h4>
                            </div>

                            <div class="desc">
                                <p><?=  $task->description  ?><small class="opacity"> - popisok</small></p>
                            </div>

                            <?php
                            if ($task->file!="uploads/") {
                                ?>
                                <span class="file">
                                    <a href="<?= $task->file ?>"><?= $task->file ?><small class="opacity"> - súbor</small></a>
                                </span>
                                <?php
                            }
                            ?>

                            <?php
                            if ($task->deadline!="0000-00-00 00:00:00") {
                                ?>
                                <div class="deadline float-right">
                                    <p class="d-inline-block">Deadline: <?=  $task->deadline   ?></p>
                                </div>

                                <?php
                            }
                            ?>


                        </div>

                        <?php
                    }

                }



                ?>
            </div>
            <div class="edit">
                <div class="addForm">

                    <?php
                    if (isset($_POST['edit'])) {
                        include "inc/errors.php" ;
                    }
                    ?>

                    <form class="addNew" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="userID" value="<?php echo  $userID ?>">
                        <input type="text" placeholder="Názov" name="name" value="<?=  $task->name  ?>" required>
                        <textarea placeholder="Popisok" name="description" rows="3" ><?=  $task->description  ?></textarea>
                        <input type="hidden" name="oldTime" value="<?=  $task->deadline ?>">
                        <input type="datetime-local" name="deadline">
                        <input type="hidden" name="oldFile" value="<?=  $task->file ?>">
                        <input type="file" name="newFile" id="fileToUpload">
                        <input type="submit" name="edit" value="Edit">
                    </form>

                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
</div>

<?php include "inc/footer.php";


