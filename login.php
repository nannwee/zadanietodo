<?php require "inc/config.php" ?>

<?php require "inc/header.php" ?>

<?php require "inc/register.php" ?>

<div class="login-page">
    <div class="form">

<?php
            include "inc/errors.php" ;
?>

        <form class="login-form" method="post" action="login.php">
            <input type="email" placeholder="Email" name="email">
            <input type="password" placeholder="Heslo" name="password">
            <input type="submit" name="login" value="Login">
            <p class="message">Not registered? <a href="index.php">Create an account</a></p>
        </form>
    </div>
</div>


<?php   include "inc/footer.php"    ?>
