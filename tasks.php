
<?php require "inc/config.php" ?>

<?php include "inc/register.php" ?>

<?php
    if (isset($_POST['add'])){
        include "inc/addNew.php";
    }

    $userID = "";

    $sql = "SELECT id,email from users where email = '$_SESSION[user]'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0){
        while ($row = $result->fetch_assoc()){
            $userID = $row["id"];
        }
    }



?>

<?php include "inc/header.php" ?>

<nav class="navbar navbar-expand-md fixed-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarRes" >
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarRes">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php?logout='1'">logout</a>
            </li>
        </ul>
    </div>
</nav>

<div id="top">

<?php

if (isset($_SESSION['success'])){

    ?>
    <div class="error-success">
        <h3>
            <?php
            echo $_SESSION['success'];
            unset($_SESSION['success']);
            ?>
        </h3>
    </div>
    <?php
}
if (isset($_SESSION['user'])){




?>


<h2>Welcome <?php  echo $_SESSION['user']   ?> </h2>



<div class="container-fluid ">



    <div class="content">
        <div class="tasks">

<?php

                $sql = "SELECT taskID,userID,`name`,`description`,deadline,file,email from tasks join users on ( id = userID ) where email = '$_SESSION[user]'";
                $data = $conn->query($sql);


                if ($data->num_rows > 0) {
                    while ($row = $data->fetch_assoc()) {
?>
                        <div class="task mb-4">
                            <div class="nazov col-md-12 mb-3">
                                <h4><strong><?=  $row["name"] ?></strong></h4>
                            </div>

                            <div class="desc">
                                <p><?=  $row["description"]  ?></p>
                            </div>

                            <?php
                                if ($row["file"]!="uploads/") {
                            ?>
                                <span class="file">
                                    <a href="<?= $row["file"] ?>">Click here to open attached file</a>
                                </span>
                            <?php
                                }
                            ?>

                            <?php
                                if ($row["deadline"]!="0000-00-00 00:00:00") {
                            ?>
                                <div class="deadline float-right">
                                    <p class="d-inline-block">Deadline: <?=  $row["deadline"]   ?></p>
                                </div>

                                <br>
                            <?php
                                }
                            ?>



                            <div class="editTask float-right mt-1">
                                <a href="edit.php?id=<?=  $row["taskID"] ?>" class="edit-link mx-1">edit</a>

                                <a href="inc/delete.php?id=<?=  $row["taskID"]  ?>" class="delete-link glyphicon glyphicon-remove text-danger mx-1 deleteTask" ></a>
                            </div>

                        </div>

                        <?php
                    }
                }


            }



?>
        </div>
        <div class="edit">
                <div class="addForm">

                    <?php
                    if (isset($_POST['add'])) {
                        include "inc/errors.php" ;
                    }
                    ?>

                    <form class="addNew" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="userID" value="<?php echo  $userID ?>">
                        <input type="text" placeholder="Názov" name="name" required >
                        <textarea placeholder="Popisok" name="description" rows="3"></textarea>
                        <input type="datetime-local" name="deadline" >
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <input type="submit" name="add" value="Add new">
                    </form>

                </div>
        </div>
        <div style="clear: both"></div>
    </div>
</div>
</div>

<?php include "inc/footer.php";

