<?php

session_start();

$errors = array();
$emails_all = array();

$sql = "SELECT email from users";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        array_push($emails_all,$row['email']);
    }
}

require "User.php";

// REGISTRÁCIA

if (isset($_POST['send']) ){


    $user = new User();
    $user->registerDataSet($_POST["email"],$_POST["password"],$_POST["firstName"],$_POST["lastName"]);

    foreach ($emails_all as $email_one){
        if ($user->email_r == $email_one){
            array_push($errors,"Email is already taken");
        }
    }

    if (empty($user->email_r) || ($user->checkEmail($user->email_r)) == false){
        array_push($errors,"Please enter valid email");
    }



    if ($user->checkPassword($user->password_r)){
        array_push($errors,"Please enter valid password");
    }

    if (empty($user->firstname)){
        array_push($errors,"Please enter valid firstname");
    }

    if (empty($user->lastname)){
        array_push($errors,"Please enter valid lastname");
        echo "success";
    }

    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $last_url = end($link_array);

    if (sizeof($errors) <= 0 && $last_url == "index.php"){
        $password_en = md5($user->password_r);
        $sql = "INSERT INTO users (email,password,firstname,lastname) values ('$user->email_r','$password_en','$user->firstname','$user->lastname')";

        $conn->query($sql);

        $_SESSION['user'] = $user->email_r;
        $_SESSION['success'] = "Logged in successfully";

        header("Location: tasks.php");

    }
}

//  PRIHLÁSENIE

if (isset($_POST['login'])){
    $user = new User();
    $user->loginDataSet($_POST['email'],$_POST['password']);

    if (empty($user->email_l)){
        array_push($errors,"Please enter your email");
    }

    if (empty($user->password_l)){
        array_push($errors,"Please enter your password");
    }

    if (sizeof($errors) <= 0){
        $password_en = md5($user->password_l);

        $sql = "SELECT * FROM users where email = '$user->email_l' and password = '$password_en'";
        $result = $conn->query($sql);
        if ($result->num_rows == 1){
            $_SESSION['user'] = $user->email_l;
            $_SESSION['success'] = "Logged in successfully";

            header("Location: tasks.php");
        }else{
            array_push($errors,"Wrong email/password");
            header("Location: login.php");
        }


    }

}



if (isset($_GET['logout'])){
    session_destroy();
    header("location: index.php");
}