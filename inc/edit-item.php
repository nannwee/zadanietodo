<?php

require "config.php";

$target_dir = "uploads/" . $_POST['userID'];
$file = "";



$errors_edit = array();


// ÚPRAVA ÚLOHY


if (isset($_POST["edit"])) {

    if (strlen($_POST["name"]) <= 0){
        array_push($errors_edit,"Please name your task");
    }

    if (isset($_FILES["newFile"]) && $_FILES["newFile"]["size"] > 0) {
        $file = $target_dir . basename($_FILES["newFile"]["name"]);

        $imageFileType = strtolower(pathinfo($file,PATHINFO_EXTENSION));


        if (isset($_FILES["newFile"]) && $_FILES["newFile"]["size"] > 0){
            if (file_exists($file)){
                array_push($errors_edit,"Sorry file already exists<br>");
            }
            if ($_FILES["newFile"]["size"] > 600000){
                array_push($errors_edit,"Sorry, your file is too large<br>");
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "gif" && $imageFileType != "jpeg"){
                array_push($errors_edit,"Wrong format<br>");
            }
            if (sizeof($errors_edit) > 0){
                array_push($errors_edit,"Sorry, your file was not uploaded<br>");
            }else{
                if (move_uploaded_file($_FILES["newFile"]["tmp_name"],$file)){
                    unlink( $_POST["oldFile"]);
                }else{
                    array_push($errors_edit,"Sorry, there was an error uploading your file<br>");
                }
            }
        }
    } else {
        $file = $_POST["oldFile"];
    }


    if (sizeof($errors_edit) <= 0){

        $deadline = date(strtotime($_POST['oldTime']));

        if ($_POST['deadline'] != null){
            $deadline = date("Y-m-d H:i:s",strtotime($_POST['deadline']));
        }

        $name = mysqli_real_escape_string($_POST['name']);

        $sql = "UPDATE tasks SET `name` = '$_POST[name]', `description` = '$_POST[description]', deadline = '$deadline', file = '$file' where taskID = '$_GET[id]'";
        $result = $conn->query($sql);


        header("Location: tasks.php");
        die();

    }



}



